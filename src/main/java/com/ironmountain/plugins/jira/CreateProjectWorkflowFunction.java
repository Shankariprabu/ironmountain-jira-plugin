package com.ironmountain.plugins.jira;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;

import com.atlassian.crowd.embedded.api.User;
import com.atlassian.jira.ComponentManager;
import com.atlassian.jira.avatar.AvatarManager;
import com.atlassian.jira.bc.project.ProjectService;
import com.atlassian.jira.config.ConstantsManager;
import com.atlassian.jira.config.IssueTypeManager;
import com.atlassian.jira.issue.CustomFieldManager;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.IssueManager;
import com.atlassian.jira.issue.MutableIssue;
import com.atlassian.jira.issue.context.manager.JiraContextTreeManager;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.issue.fields.FieldManager;
import com.atlassian.jira.issue.fields.config.manager.FieldConfigSchemeManager;
import com.atlassian.jira.issue.fields.config.manager.IssueTypeSchemeManager;
import com.atlassian.jira.permission.PermissionSchemeManager;
import com.atlassian.jira.project.AssigneeTypes;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.project.ProjectManager;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.util.ErrorCollection;
import com.atlassian.jira.util.SimpleErrorCollection;
import com.atlassian.jira.workflow.function.issue.AbstractJiraFunctionProvider;
import com.atlassian.plugin.webresource.WebResourceUrlProvider;
import com.atlassian.sal.api.message.I18nResolver;
import com.opensymphony.module.propertyset.PropertySet;
import com.opensymphony.workflow.WorkflowException;

public class CreateProjectWorkflowFunction extends AbstractJiraFunctionProvider {

	private final ProjectService projectService;
	private final ProjectManager projectManager;
	private final JiraAuthenticationContext authContext;

	public CreateProjectWorkflowFunction(ProjectService projectService,
			ProjectManager projectManager, JiraAuthenticationContext authContext) {

		this.projectManager = projectManager;
		this.projectService = projectService;
		this.authContext = authContext;

	}

	@Override
	public void execute(Map transientVars, Map args, PropertySet ps)
			throws WorkflowException {
		
		MutableIssue issue = getIssue(transientVars);
		
		String userName = (String) args.get("user");

		// get reference to the IssueManager and CustomFieldManager manager classes
		//IssueManager issueManager = ComponentManager.getInstance().getIssueManager();
		CustomFieldManager customFieldManager = ComponentManager.getInstance().getCustomFieldManager();
		 
		// get reference to the project key custom field
		CustomField projectKeyCF = customFieldManager.getCustomFieldObjectByName( "Project Key" );
		CustomField projectNameCF = customFieldManager.getCustomFieldObjectByName( "Project Name" );
		 
		// retrieves the custom field value object from the issue
		Object projectKeyCFValue = issue.getCustomFieldValue( projectKeyCF );
		Object projectNameCFValue = issue.getCustomFieldValue( projectNameCF );
		 
		// prints value to console
		System.out.println( projectKeyCFValue );		
		System.out.println( projectNameCFValue );		
		
/////////////
		
		// validateCreateProject(com.atlassian.crowd.embedded.api.User user,
		// String name, String key, String description, String lead, String url,
		// Long assigneeType, Long avatarId)
		// validateCreateProject(com.atlassian.crowd.embedded.api.User user,
		// String name, String key, String description, String lead, String url,
		// Long assigneeType)
		System.out.println("CREATING: " + issue.getSummary());

		User user = authContext.getLoggedInUser();
		String projectName = "THIS IS THE PROJECT NAME";
		String projectKey = "XXX";
		String description = "THIS IS THE PROJECT DESCRIPTION";
		String lead = "admin";
		String url = null;
		Long assigneeType = AssigneeTypes.PROJECT_LEAD;

		ProjectService.CreateProjectValidationResult validation = projectService
				.validateCreateProject(user, projectName, projectKey,
						description, lead, url, assigneeType);

		ErrorCollection errorCollection = new SimpleErrorCollection();
		errorCollection.addErrorCollection(validation.getErrorCollection());

		if (!errorCollection.hasAnyErrors()) {

			Project project = projectService.createProject(validation);
		} else {
			System.out.println(errorCollection.getErrorMessages());
			// ArrayList<?> list = (ArrayList)
			// errorCollection.getErrorMessages();
			// for (int i = 0; i < list.size(); i++){
			// System.out.println("ERROR: " + list.get(i));
			// }
		}
	}

}
