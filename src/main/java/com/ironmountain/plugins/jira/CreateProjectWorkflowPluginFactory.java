package com.ironmountain.plugins.jira;

import java.util.Map;

import com.atlassian.core.util.map.EasyMap;
import com.atlassian.jira.plugin.workflow.AbstractWorkflowPluginFactory;
import com.atlassian.jira.plugin.workflow.WorkflowPluginFunctionFactory;
import com.opensymphony.workflow.loader.AbstractDescriptor;

public class CreateProjectWorkflowPluginFactory extends
		AbstractWorkflowPluginFactory implements WorkflowPluginFunctionFactory {

    private static final String USER_NAME = "user";
    private static final String CURRENT_USER = "Current User";

    @Override
    protected void getVelocityParamsForEdit(Map velocityParams, AbstractDescriptor descriptor) {
        velocityParams.put(USER_NAME, getUserName(descriptor));
    }

    @Override
    protected void getVelocityParamsForInput(Map velocityParams) {
        velocityParams.put(USER_NAME, CURRENT_USER);
    }

    @Override
    protected void getVelocityParamsForView(Map velocityParams, AbstractDescriptor descriptor) {
        velocityParams.put(USER_NAME, getUserName(descriptor));
    }

    @SuppressWarnings("unchecked")
    public Map getDescriptorParams(Map conditionParams) {
        if (conditionParams != null && conditionParams.containsKey(USER_NAME))  {
            return EasyMap.build(USER_NAME, extractSingleParam(conditionParams, USER_NAME));
        }
        // Create a 'hard coded' parameter
        return EasyMap.build(USER_NAME, CURRENT_USER);
    }   

    private String getUserName(AbstractDescriptor descriptor){
        return "admin";
    }

}
